/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

/**
 * Es una clase que se utilza para representar los caracteres para general el
 * árbol de huffman y codificación en binario.
 *
 * @author Ricardo, Brayan,Victor
 */
public class Caracteres {

    int cantidad;//ocasiones repetidas
    char letra;//caracter que se repite
    String codigo;//codigo binario que representa la letra

    public Caracteres() {
    }

    public Caracteres(int cantidad, char letra) {
        this.cantidad = cantidad;
        this.letra = letra;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    @Override
    public String toString() {
        return letra + "( " + cantidad + " )";
    }

}
