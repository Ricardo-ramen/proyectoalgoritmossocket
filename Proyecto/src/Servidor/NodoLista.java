/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

/**
 *
 * @author Ricardo,Brayan,Victor
 */
public class NodoLista {

    NodoLista siguiente;
    NodoArbol dato;

    public NodoLista(NodoArbol datos) {
        this.dato = datos;
    }

    public NodoLista() {
    }

    public NodoLista getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoLista siguiente) {
        this.siguiente = siguiente;
    }

    public NodoArbol getDato() {
        return dato;
    }

    public void setDato(NodoArbol datos) {
        this.dato = datos;
    }
}
