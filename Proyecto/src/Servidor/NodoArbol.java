/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

/**
 *
 * @author Ricardo,Brayan,Victor
 */
public class NodoArbol {

    NodoArbol izquierdo;
    NodoArbol derecho;
    Caracteres caracteres;

    public NodoArbol() {
    }

    public NodoArbol(Caracteres caracteres) {
        this.caracteres = caracteres;
    }

    public NodoArbol(NodoArbol izquierdo, NodoArbol derecho) {
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }

    public NodoArbol getIzquierdo() {
        return izquierdo;
    }

    public void setIzquierdo(NodoArbol izquierdo) {
        this.izquierdo = izquierdo;
    }

    public NodoArbol getDerecho() {
        return derecho;
    }

    public void setDerecho(NodoArbol derecho) {
        this.derecho = derecho;
    }

    public Caracteres getCaracteres() {
        return caracteres;
    }

    public void setCaracteres(Caracteres caracteres) {
        this.caracteres = caracteres;
    }

}
