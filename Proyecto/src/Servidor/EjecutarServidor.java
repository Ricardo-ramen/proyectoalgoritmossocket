/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.io.PrintStream;
import java.net.Socket;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Ricardo,Brayan,Victor
 */
public class EjecutarServidor {

    ManejadorArchivos leer = new ManejadorArchivos();
    File fichero;
    String ip = "";
    EncrIptarMD5 md5 = new EncrIptarMD5();
    String codigoMD5 = "";
    String mensajeS = "";
    String mensajesC = "";
    String mm = "";

    public String enviar() {

        JFileChooser cargar = new JFileChooser();

        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.TXT", "txt");

        cargar.setFileFilter(filtro);

        int seleccion = cargar.showOpenDialog(cargar);

        if (seleccion == JFileChooser.APPROVE_OPTION) {

            fichero = cargar.getSelectedFile();

        }

        try {
            ServerSocket servidor = new ServerSocket(4500);

            Socket clienteNuevo = servidor.accept();

            PrintStream mensaje = new PrintStream(clienteNuevo.getOutputStream());

            FileInputStream origen = new FileInputStream(fichero);

            byte[] buffer = new byte[1024];

            int len;

            while ((len = origen.read(buffer)) > 0) {
                mensaje.write(buffer, 0, len);
            }

            JOptionPane.showMessageDialog(null, "Mensaje Enviado");

            servidor.close();
            clienteNuevo.close();

            return leer.leerTexto(fichero.getAbsolutePath());

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + ex.getMessage());
        }
        return "";
    }

    public void recbir(String ip) {

        this.ip = ip;

        ManejadorArchivos leer = new ManejadorArchivos();
        String cadena = "";

        JFileChooser guardar = new JFileChooser();

        FileNameExtensionFilter filtro2 = new FileNameExtensionFilter("*.TXT", "txt");

        guardar.setFileFilter(filtro2);

        int seleccion = guardar.showSaveDialog(guardar);

        if (seleccion == JFileChooser.APPROVE_OPTION) {

            cadena = guardar.getSelectedFile().getAbsolutePath();

        }
        try {
            Socket cliente = new Socket(ip, 4500);

            InputStream entrada = cliente.getInputStream();

            FileOutputStream mensaje = new FileOutputStream(cadena);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = entrada.read(buffer)) > 0) {
                mensaje.write(buffer, 0, len);
            }

            String texto = leer.leerTexto(cadena);

            codigoMD5 = md5.Encriptar(texto);

            cliente.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + ex.getMessage()
                    + "\n El servidor con la ip " + ip + " no está enviando un archivo");
        }
    }

    public boolean verificarMD5Servidor(String md5) {

        try {

            ServerSocket servidor = new ServerSocket(3500);

            Socket clienteN = servidor.accept();

            BufferedReader mensaje = new BufferedReader(new InputStreamReader(clienteN.getInputStream()));

            mensajeS = mensaje.readLine();

            mensajeS = mensajeS.trim();

            if (mensajeS.equals(md5)) {
                servidor.close();
                clienteN.close();
                return true;
            } else {
                servidor.close();
                clienteN.close();
                return false;
            }

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + ex.getMessage());
        }
        return false;
    }

    public void verificarMD5Cliente() {
        try {
            Socket cliente = new Socket(ip, 3500);

            DataOutputStream mensajeEnviado = new DataOutputStream(cliente.getOutputStream());

            JOptionPane.showMessageDialog(null, "Su código MD5 es: " + codigoMD5);

            mensajeEnviado.writeUTF(codigoMD5);

            cliente.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + ex.getMessage()
                    + "\n Primero debe enviar o recibir un archivo.");
        }
    }

}
