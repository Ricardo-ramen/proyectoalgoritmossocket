package Servidor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Ricardo,Brayan,Victor
 */
public class Menu {

    static File fichero;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ExcpArbol, IOException {

        ManejadorArchivos leer = new ManejadorArchivos();
        EjecutarServidor ejecutar = new EjecutarServidor();

        JFileChooser CargarArchivo = new JFileChooser();
        JFileChooser GuardarArchivoCodificado = new JFileChooser();
        JFileChooser CargarArchivoCodificado = new JFileChooser();
        JFileChooser GuardarArchivoDecodificado = new JFileChooser();

        File fichero = null;
        Huffman arbol = new Huffman();
        ArrayList<Codigo> listaCodigos = new ArrayList<>();
        String texto = "";
        String codificado = "";
        FileNameExtensionFilter filtro;
        EncrIptarMD5 md5 = new EncrIptarMD5();
        String codigoMD5 = "";
        String ip = "";
        boolean seguir = true;

        while (true) {
            String opcion = JOptionPane.showInputDialog("1: Cargar Archivo \n2: Comprimir archivo y Generar MD5  \n3: Enviar archivo"
                    + "\n4: Recibir archivo \n5: Descomprimir archivo \n6: Salir");

            switch (opcion) {
                case "1":
                    try {
                        filtro = new FileNameExtensionFilter("*.TXT", "txt");
                        CargarArchivo.setFileFilter(filtro);
                        int seleccion = CargarArchivo.showOpenDialog(CargarArchivo);

                        if (seleccion == JFileChooser.APPROVE_OPTION) {
                            fichero = CargarArchivo.getSelectedFile();
                        }
                        arbol.asignar(fichero.getAbsolutePath());
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Se canceló la acción");
                    }

                    break;
                case "2":
                    try {

                        arbol.insertarTexto();

                        listaCodigos = arbol.codificar();

                        codificado = arbol.codificarTexto(texto);

                        JOptionPane.showMessageDialog(null, "Guardar Archivo Codificado");

                        try {
                            FileNameExtensionFilter filtro2 = new FileNameExtensionFilter("*.TXT", "txt");
                            GuardarArchivoCodificado.setFileFilter(filtro2);

                            String cadena = "";

                            int seleccion = GuardarArchivoCodificado.showSaveDialog(GuardarArchivoCodificado);

                            if (seleccion == JFileChooser.APPROVE_OPTION) {

                                cadena = GuardarArchivoCodificado.getSelectedFile().getAbsolutePath();

                            }

                            leer.escribirEnviar(codificado, listaCodigos, cadena);

                            codigoMD5 = md5.Encriptar(leer.leerTexto(cadena));

                            JOptionPane.showMessageDialog(null, "El código MD5 generado es :" + codigoMD5);

                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Se canceló la acción");
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + e.getMessage());
                    }
                    break;
                case "3":
                    try {
                        codigoMD5 = md5.Encriptar(ejecutar.enviar());

                        if (ejecutar.verificarMD5Servidor(codigoMD5) == true) {
                            JOptionPane.showMessageDialog(null, "Los códigos MD5 son iguales");
                        } else {
                            JOptionPane.showMessageDialog(null, "Los códigos MD5 no concuerdan");
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + e.getMessage());
                    }

                    break;
                case "4":
                    try {
                        ip = JOptionPane.showInputDialog(null, "Digite la dirección ip del destinatario");
                        while (seguir) {
                            if (correcta(ip) == true) {
                                seguir = false;
                            } else {
                                JOptionPane.showMessageDialog(null, "La ip no cumple con los requisitos. Vuelva a digitarla");
                                ip = JOptionPane.showInputDialog(null, "Digite la dirección ip del destinatario");
                            }
                        }
                        ejecutar.recbir(ip);
                        ejecutar.verificarMD5Cliente();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + e.getMessage());
                    }

                    break;
                case "5":

                    try {

                        listaCodigos = new ArrayList<>();

                        JOptionPane.showMessageDialog(null, "Cargar Archivo Codificado");

                        FileNameExtensionFilter filtro3 = new FileNameExtensionFilter("*.TXT", "txt");

                        CargarArchivoCodificado.setFileFilter(filtro3);

                        int seleccion = CargarArchivoCodificado.showOpenDialog(CargarArchivoCodificado);

                        if (seleccion == JFileChooser.APPROVE_OPTION) {

                            fichero = CargarArchivoCodificado.getSelectedFile();

                        }

                        texto = leer.leerSoloTexto(fichero.getAbsolutePath());

                        listaCodigos = leer.crearLista(fichero.getAbsolutePath());

                        texto = arbol.decodificarTexto(texto, listaCodigos);

                        JOptionPane.showMessageDialog(null, "Guardar Archivo Decodificado");

                        FileNameExtensionFilter filtro4 = new FileNameExtensionFilter("*.TXT", "txt");
                        GuardarArchivoDecodificado.setFileFilter(filtro4);

                        String cadena = "";

                        int seleccion1 = GuardarArchivoDecodificado.showSaveDialog(GuardarArchivoDecodificado);

                        if (seleccion1 == JFileChooser.APPROVE_OPTION) {

                            cadena = GuardarArchivoDecodificado.getSelectedFile().getAbsolutePath();

                        }

                        leer.escribir(texto, cadena);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Se canceló la acción por: " + e.getMessage());
                    }

                    break;
                case "6":
                    System.exit(0);
                    break;
                default:
                    break;

            }
        }

    }

    private static boolean correcta(String ip) {

        String a = ip;

        a = a.replace(".", ",");

        String[] separar = a.split(",");

        if (ip.equalsIgnoreCase("localhost")) {
            return true;
        }
        if (separar.length > 4 || separar.length < 4) {
            return false;
        }
        for (int i = 0; i < separar.length; i++) {
            if (separar[i].length() > 3) {
                return false;
            }
        }
        return true;
    }
}
