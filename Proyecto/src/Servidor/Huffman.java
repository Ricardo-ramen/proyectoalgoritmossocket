package Servidor;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Clase encargada de crear el arbol de caracteres.Además codifica y descodifica
 * el texto.
 *
 * @author Ricardo,Brayan,Victor
 */
public class Huffman {

    NodoLista primero;
    NodoLista ultimo;
    NodoLista siguiente;
    NodoLista nodoLista;
    NodoArbol nodoArbol;
    String textoDelArchivo = "";
    String textoEnOrden = "";//en caso de impresion
    ArrayList<Codigo> codigos = new ArrayList<Codigo>();
    ManejadorArchivos leer = new ManejadorArchivos();

    /**
     * Método que lee la ruta del archivo a manipular
     *
     * @param ruta ruta del archivo
     * @throws IOException
     */
    public void asignar(String ruta) throws IOException {
        this.textoDelArchivo = leer.leerTexto(ruta);
    }

    /**
     * Método publico que realiza un llamado al método privado encargado la
     * insercción de caracteres
     *
     * @param dato caracter a insertar, por defecto la cantidad repetida es de
     * uno.
     * @throws ExcpArbol
     */
    public void insertarTexto() throws ExcpArbol {
        for (int i = 0; i < textoDelArchivo.length(); i++) {
            insertar(textoDelArchivo.charAt(i), 1);
        }
        crearArbolComprimido();
    }

    /**
     * Inserta en el nodo del árbol la cantidad de veces que se repite una
     * letra, a si como su caracter correspondiente
     *
     * @param dato caracter a insertar.
     * @param cantidad cantidad de veces repetido un caracter.
     * @throws ExcpArbol
     */
    private void insertar(char dato, int cantidad) throws ExcpArbol {
        NodoLista nuevo;
        nuevo = new NodoLista(new NodoArbol(new Caracteres(cantidad, dato)));

        if (primero == null) {
            primero = nuevo;
            ultimo = nuevo;
        } else if (existe(nuevo) == true) {
            nuevo.getDato().getCaracteres().setCantidad(valor(nuevo) + 1);
            if (nuevo.getDato().getCaracteres().getCantidad() >= ultimo.getDato().getCaracteres().getCantidad()) {
                ultimo.setSiguiente(nuevo);
                ultimo = nuevo;
                eliminarRepetido(nuevo);
            } else if (nuevo.getDato().getCaracteres().getCantidad() <= primero.getDato().getCaracteres().getCantidad()) {
                nuevo.setSiguiente(primero);
                primero = nuevo;
                eliminarRepetido(nuevo);
            }
            for (NodoLista actual = primero; actual.getSiguiente() != null; actual = actual.getSiguiente()) {

                if (nuevo.getDato().getCaracteres().getCantidad() < actual.getSiguiente().getDato().getCaracteres().getCantidad()) {
                    nuevo.setSiguiente(actual.getSiguiente());
                    actual.setSiguiente(nuevo);
                    eliminarRepetido(nuevo);
                    break;
                }
            }
        } else {
            if (nuevo.getDato().getCaracteres().getCantidad() >= ultimo.getDato().getCaracteres().getCantidad()) {
                ultimo.setSiguiente(nuevo);
                ultimo = nuevo;
            } else if (nuevo.getDato().getCaracteres().getCantidad() <= primero.getDato().getCaracteres().getCantidad()) {
                nuevo.setSiguiente(primero);
                primero = nuevo;
            }
        }

    }

    public int valor(NodoLista nuevo) {
        for (NodoLista actual = primero; actual != null; actual = actual.getSiguiente()) {
            if (actual.getDato().getCaracteres().getLetra() == nuevo.getDato().getCaracteres().getLetra()) {
                return actual.getDato().getCaracteres().getCantidad();
            }
        }
        return 0;
    }

    /**
     * Método que recorre el árbol para asignar la cantidad
     *
     * @param nuevo nodo del árbol
     * @return true si esta el valor en el árbol o false en caso contrario.
     */
    public boolean existe(NodoLista nuevo) {
        for (NodoLista actual = primero; actual != null; actual = actual.getSiguiente()) {
            if (String.valueOf(nuevo.getDato().getCaracteres().getLetra()).equals(String.valueOf(actual.getDato().getCaracteres().getLetra()))) {
                return true;
            }
        }
        return false;
    }

    public void eliminarRepetido(NodoLista nuevo) {
        if (primero.getDato().getCaracteres().getLetra() == nuevo.getDato().getCaracteres().getLetra()) {
            primero = primero.getSiguiente();
        } else {
            for (NodoLista actual = primero; actual != null; actual = actual.getSiguiente()) {
                if (actual.getSiguiente().getDato().getCaracteres().getLetra() == nuevo.getDato().getCaracteres().getLetra() && actual.getSiguiente() != null) {
                    if (actual.getSiguiente() == ultimo) {
                        actual.setSiguiente(null);
                        ultimo = actual;
                        break;
                    } else {
                        actual.setSiguiente(actual.getSiguiente().getSiguiente());
                        break;
                    }
                }
            }
        }
    }

    public void crearArbolComprimido() throws ExcpArbol {
        crearArbol();
    }

    private void crearArbol() throws ExcpArbol {
        if (primero == ultimo) {

        } else {
            insertarArbol(primero.getDato(), primero.getSiguiente().getDato());
            primero = primero.getSiguiente().getSiguiente();
            crearArbol();

        }
    }

    private void insertarArbol(NodoArbol nuevo, NodoArbol nuevo2) throws ExcpArbol {

        nodoArbol = new NodoArbol(nuevo, nuevo2);

        int suma = 0;

        suma = suma + (nuevo.getCaracteres().getCantidad() + nuevo2.getCaracteres().getCantidad());

        nodoLista = new NodoLista(nodoArbol);
        nodoLista.getDato().setCaracteres(new Caracteres(suma, ' '));

        if (ultimo.getDato().getCaracteres().getCantidad() <= nodoLista.getDato().getCaracteres().getCantidad()) {
            ultimo.setSiguiente(nodoLista);
            nodoLista.setSiguiente(null);
            ultimo = nodoLista;
        } else {

            for (NodoLista actual = primero; actual.getSiguiente() != null; actual = actual.getSiguiente()) {
                if (actual.getSiguiente().getDato().getCaracteres().getCantidad() == nodoLista.getDato().getCaracteres().getCantidad()) {
                    nodoLista.setSiguiente(actual.getSiguiente());
                    actual.setSiguiente(nodoLista);
                    break;
                } else if (actual.getSiguiente().getDato().getCaracteres().getCantidad() > nodoLista.getDato().getCaracteres().getCantidad()) {
                    nodoLista.setSiguiente(actual.getSiguiente());
                    actual.setSiguiente(nodoLista);
                    break;
                }

            }
        }

    }

    public ArrayList<Codigo> codificar() throws ExcpArbol {
        if (vacio()) {
            throw new ExcpArbol("No existe ningun elemento. Primero debe cargar un archivo de texto");
        } else {
            if (primero.getDato().getIzquierdo() == null && primero.getDato().getDerecho() == null) {
                codigos.add(new Codigo(primero.getDato().getCaracteres().getLetra(), "0"));
                return codigos;
            } else {
                textoEnOrden = "";
                primero.getDato().getCaracteres().setCodigo("");
                return codigos(primero.getDato());
            }
        }
    }

    private ArrayList<Codigo> codigos(NodoArbol Nodoraiz) {

        if (Nodoraiz != null) {

            if (Nodoraiz.getIzquierdo() == null && Nodoraiz.getDerecho() == null) {
                codigos.add(new Codigo(Nodoraiz.getCaracteres().getLetra(), Nodoraiz.getCaracteres().getCodigo()));
            }

            if (Nodoraiz.getIzquierdo() != null) {
                Nodoraiz.getIzquierdo().getCaracteres().setCodigo(Nodoraiz.getCaracteres().getCodigo() + "0");
                codigos(Nodoraiz.getIzquierdo());
            }

            if (Nodoraiz.getDerecho() != null) {
                Nodoraiz.getDerecho().getCaracteres().setCodigo(Nodoraiz.getCaracteres().getCodigo() + "1");
                codigos(Nodoraiz.getDerecho());
            }
        }
        return codigos;
    }

    public String enOrden() throws ExcpArbol {
        if (vacio()) {
            throw new ExcpArbol("No existe ningun elemento");
        } else {
            textoEnOrden = "";
            return enOrden(primero.getDato());
        }
    }

    private String enOrden(NodoArbol Nodoraiz) {

        if (Nodoraiz != null) {
            enOrden(Nodoraiz.getIzquierdo());
            textoEnOrden += Nodoraiz.getCaracteres().toString();
            enOrden(Nodoraiz.getDerecho());
        }
        return textoEnOrden;
    }

    public String codificarTexto(String texto) {
        String textoCambiado = "";

        for (int i = 0; i < texto.length(); i++) {
            textoCambiado += cambio(texto.charAt(i));
        }

        return textoCambiado;
    }

    public String cambio(char letra) {

        for (int i = 0; i < codigos.size(); i++) {
            if (codigos.get(i).getLetra() == letra) {
                return codigos.get(i).getCifrado();
            }
        }
        return "";
    }

    public String decodificarTexto(String texto, ArrayList<Codigo> codigos) {

        String textoR = texto;
        String textoCambiado = "";
        String concatenar = "";

        for (int i = 0; i < textoR.length(); i++) {
            concatenar += texto.charAt(i);
            for (int j = 0; j < codigos.size(); j++) {
                if (codigos.get(j).getCifrado().trim().equals(concatenar)) {
                    if (codigos.get(j).getLetra() == '¶') {
                        textoCambiado += "\r\n";
                        concatenar = "";
                        break;
                    } else {
                        textoCambiado += codigos.get(j).getLetra();
                        concatenar = "";
                        break;
                    }
                }
            }
        }

        return textoCambiado;
    }

    public String imprimir() {

        String texto = "";

        for (NodoLista actual = primero; actual != null; actual = actual.getSiguiente()) {
            texto += actual.getDato().getCaracteres().toString() + " / ";

        }
        return texto;
    }

    private boolean vacio() {
        if (primero == null) {
            return true;
        }
        return false;
    }

}
