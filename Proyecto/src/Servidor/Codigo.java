/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

/**
 *Clase encargada de almacenar la información del mapa de codificacíon.
 * @author Ricardo,Brayan,Victor
 */
public class Codigo {

    char letra;
    String cifrado;

    public Codigo() {
    }

    public Codigo(char letra, String cifrado) {
        this.letra = letra;
        this.cifrado = cifrado;
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    public String getCifrado() {
        return cifrado;
    }

    public void setCifrado(String cifrado) {
        this.cifrado = cifrado;
    }

    @Override
    public String toString() {
        return letra + " = " + cifrado;
    }

}
