/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ricardo,Brayan,Victor
 */
public class EncrIptarMD5 {

    public String Encriptar(String texto) {

        try {
            MessageDigest MD5Enciptar = MessageDigest.getInstance("MD5");
            byte[] bytes = MD5Enciptar.digest(texto.getBytes());

            int tamanho = bytes.length;
            StringBuilder bulder = new StringBuilder(tamanho);
            for (int i = 0; i < tamanho; i++) {
                int u = bytes[i] & 255;
                if (u < 16) {
                    bulder.append("0").append(Integer.toHexString(u));
                } else {
                    bulder.append(Integer.toHexString(u));
                }
            }
            return bulder.toString();
        } catch (NoSuchAlgorithmException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return "";
    }

}
