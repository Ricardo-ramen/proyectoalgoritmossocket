/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Ricardo,Brayan,Victor
 */
public class ManejadorArchivos {

    public String leerTexto(String texto) throws IOException {
        String textoC = leer(texto);
        return textoC;
    }

    public String leerSoloTexto(String texto) throws IOException {
        String textoC = leerSTexto(texto);
        return textoC;
    }

    private String leer(String texto) throws FileNotFoundException, IOException {

        String cadena;
        String nuevo = "";
        BufferedReader linea = new BufferedReader(new InputStreamReader(new FileInputStream(texto), "ISO-8859-1"));
        while ((cadena = linea.readLine()) != null) {
            nuevo += cadena + "¶";
        }
        linea.close();
        return nuevo;
    }

    private String leerSTexto(String texto) throws FileNotFoundException, IOException {

        String cadena;
        String nuevo = "";
        BufferedReader linea = new BufferedReader(new InputStreamReader(new FileInputStream(texto), "ISO-8859-1"));
        while ((cadena = linea.readLine()) != null) {
            if (cadena.equals("#")) {
                while ((cadena = linea.readLine()) != null) {
                    nuevo += cadena + "\n";
                }
            }

        }
        linea.close();
        return nuevo;
    }

    public ArrayList<Codigo> crearLista(String texto) throws FileNotFoundException, IOException {

        ArrayList<Codigo> codigos = new ArrayList<>();

        FileReader f = new FileReader(texto);

        BufferedReader b = new BufferedReader(f);

        String cadena = "";

        while ((cadena = b.readLine()) != null) {
            String[] lista = cadena.split("=");

            if (cadena.equals("#")) {
                b.close();
                return codigos;
            }
            codigos.add(new Codigo(lista[0].charAt(0), lista[1]));
        }
        b.close();

        return codigos;
    }

    public void escribirEnviar(String texto, ArrayList listaCodigo, String ruta) throws IOException {
        File archivo = new File(ruta);
        try {
            BufferedWriter escribir = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(archivo, true), "UTF-8"));
            PrintWriter pw = new PrintWriter(escribir);

            for (int i = 0; i < listaCodigo.size(); i++) {
                pw.write(listaCodigo.get(i).toString() + "\r\n");

            }

            pw.write("#" + "\r\n");

            pw.write(texto);

            pw.close();
            escribir.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void escribir(String texto, String ruta) throws IOException {
        File archivo = new File(ruta);

        try {
            BufferedWriter escribir = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(archivo, true), "UTF-8"));
            PrintWriter pw = new PrintWriter(escribir);

            pw.write(texto);

            pw.close();
            escribir.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
}
